# Mobian Encryption Setup

Semi-automatically setup disk encryption for a mobian image before flashing to a phone.
Currently this requires you to bring your own osk-sdl deb. 

Alpha builds can be found at https://gitlab.com/Jarrah/osk-sdl-mobian/-/releases  
Source code is at https://salsa.debian.org/DebianOnMobile-team/osk-sdl

Thanks go to Josh Bowyer for testing and solving DNS and boot script issues.
