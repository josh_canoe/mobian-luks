ORIGLOOP=/dev/loop0
CRYPTLOOP=/dev/loop1
CRYPTROOT=mobian_cryptroot

# Sort images
cp mobian-pinephone-*.img mobian-crypt.img
sudo losetup --partscan $ORIGLOOP mobian-pinephone-*.img
sudo losetup --partscan $CRYPTLOOP mobian-crypt.img

# Encrypt root partition
sudo cryptsetup luksFormat "$CRYPTLOOP"p2
sudo cryptsetup luksOpen "$CRYPTLOOP"p2 $CRYPTROOT
sudo mkfs.ext4 /dev/mapper/$CRYPTROOT

ENCRYPTED_PART_UUID=$(lsblk -o UUID,NAME ${CRYPTLOOP}p2 | grep loop | awk '{ print $1 }')

# Setup directories
sudo mkdir /mnt/orig
sudo mkdir /mnt/target

# Mount and copy root partition content
sudo mount "$ORIGLOOP"p2 /mnt/orig
sudo mount /dev/mapper/$CRYPTROOT /mnt/target
sudo cp -ra /mnt/orig/* /mnt/target

sudo cp osk-sdl*.deb /mnt/target/
sudo cp /mnt/target/etc/resolv.conf /mnt/target/etc/resolv.conf.bak
sudo cp /etc/resolv.conf /mnt/target/etc/resolv.conf
sudo cp /usr/bin/qemu-aarch64-static /mnt/target/bin/
cat << EOF > install.sh
apt install cryptsetup libsdl2-2.0-0 libsdl2-ttf-2.0-0 -y
dpkg -i osk-sdl*.deb
# Reconfigure boot script
sed -i "s:U_BOOT_PARAMETERS=\"\(.*\)\":U_BOOT_PARAMETERS=\"\1 osk-sdl-root=/dev/disk/by-uuid/${ENCRYPTED_PART_UUID} osk-sdl-root-name=root\":" /etc/default/u-boot
sed -i "s:UUID=.*\(\t/\t.*\):/dev/mapper/root\1:" /etc/fstab
u-boot-update

# Uncomment for old images which do not use u-boot-menu
#sed -i "s:^setenv bootargs.*$:setenv bootargs console=ttyS0,115200 no_console_suspend panic=10 consoleblank=0 loglevel=7 osk-sdl-root=/dev/mmcblk\${linux_mmcdev}p\${rootpart} osk-sdl-root-name=root root=/dev/mapper/root rw splash plymouth.ignore-serial-consoles vt.global_cursor_default=0:" /boot/boot.cmd
#mkimage -T script -C none -A arm64 -d /boot/boot.cmd /boot/boot.scr
EOF
cat install.sh
sudo mv install.sh /mnt/target/install.sh

# setup chroot
sudo mount "$CRYPTLOOP"p1 /mnt/target/boot
sudo mount --bind /dev /mnt/target/dev
sudo mount --bind /sys /mnt/target/sys
sudo mount --bind /proc /mnt/target/proc

# Run install script
sudo chroot /mnt/target qemu-aarch64-static /bin/bash /install.sh




# Teardown
sudo mv /mnt/target/etc/resolv.conf.bak /mnt/target/etc/resolv.conf
sudo umount -R /mnt/target
sudo umount /mnt/orig
sudo cryptsetup close /dev/mapper/$CRYPTROOT
sudo losetup -d $CRYPTLOOP
sudo losetup -d $ORIGLOOP

echo "mobian-crypt.img is now ready to be flashed"